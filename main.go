package main

import "fmt"

func getAnonimFunction(num1 int, num2 int, action string) func() int {
	switch action {
	case "+":
		return func() int {
			return num1 + num2

		}
	case "-":
		return func() int {
			return num1 - num2

		}
	case "*":
		return func() int {
			return num1 * num2

		}
	case "/":
		return func() int {
			return num1 / num2

		}

	}

	return func() int {
		fmt.Println("this operation not found")
		return 0
	}

}

func main() {
	function := getAnonimFunction(9, 4,"-")
	fmt.Println(function())

}
